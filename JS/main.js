"use strict";

const tabs = document.getElementsByClassName("tab");
const tabsContent = document.getElementsByClassName("tab-content");

for (let index = 0; index < tabs.length; index++) {
  tabs[index].addEventListener("click", function () {
    for (let index = 0; index < tabs.length; index++) {
      tabs[index].classList.remove("active");
    }
    tabs[index].classList.add("active");
    const tabActive = tabs[index].getAttribute("data-tab-active");
    for (let index = 0; index < tabsContent.length; index++) {
      tabsContent[index].classList.remove("active");
      for (let index = 0; index < tabsContent.length; index++) {
        if (tabsContent[index].classList.contains(`${tabActive}`)) {
          tabsContent[index].classList.add("active");
        };
      };
    };
  });
}








//-------------------------------------------------------------
const items = document.getElementsByClassName("hoverblock-parag"),
 categ = document.getElementsByClassName("works-list_item"),
 item = document.getElementsByClassName("img"),

 btn = document.getElementById("work-btn"),
 imgDownload = document.getElementsByClassName("img-download"),
 anim = document.getElementById("anim"),
 img1 = document.getElementsByClassName("img1"),
 content = document.getElementsByClassName("hoverblock-parag1");

for (let index = 0; index < categ.length; index++) {
  categ[index].addEventListener("click",function(){
    for (let index = 0; index < categ.length; index++) {
      categ[index].classList.remove("active"); 
    };
    categ[index].classList.add("active");
      for (let i = 0; i < items.length; i++) {
        if(categ[index].textContent === items[i].textContent || categ[index].textContent === categ[0].textContent){
          item[i].classList.remove("nonactive");
        }else{
          item[i].classList.add("nonactive");
        };
      };
      for (let i = 0; i < content.length; i++) {
        if(categ[index].textContent === content[i].textContent || categ[index].textContent === categ[0].textContent){
          img1[i].classList.remove("nonactive");
        }else{
          img1[i].classList.add("nonactive");
        };
      };
});
  btn.addEventListener("click",function(){
    btn.remove();
    anim.classList.add("active");
    setTimeout(function(){
      for (let index = 0; index < img1.length; index++) {
        img1[index].classList.remove("novisible");
        anim.classList.remove("active");
      for (let index = 0; index < imgDownload.length; index++) {
        imgDownload[index].classList.add("work-img")
        imgDownload[index].src = imgDownload[index].getAttribute("data-img");
      }
    }
    },2000);
  });
};

const rad = document.getElementsByClassName("manual-btn");
const btns = document.getElementsByClassName("nav-btn");
let i=0;

for (let index = 0; index < rad.length; index++) {
  rad[index].addEventListener("click",function(){
    for (let index = 0; index < rad.length; index++) {
      rad[index].classList.remove("active");
    };
    rad[index].classList.add("active");
    i = index;
  });
};

btns[0].addEventListener("click",function(){
  for (let index = 0; index < rad.length; index++) {
    rad[index].classList.remove("active");
  };
  if(i<1){i=3;}else{i--};
  const atrib = `radio${i+1}`;
  console.log(atrib);
  btns[0].setAttribute("for", atrib);
  console.log(btns[0].getAttribute("for"));
  rad[i].classList.add("active"); 
});
btns[1].addEventListener("click",function(){
  for (let index = 0; index < rad.length; index++) {
    rad[index].classList.remove("active");
  };
  if(i>2){i=0;}else{i++};
  const atrib = `radio${i+1}`;
  console.log(atrib);
  btns[1].setAttribute("for", atrib);
  console.log(btns[0].getAttribute("for"));
  rad[i].classList.add("active");
});

  